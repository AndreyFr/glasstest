import React from 'react';
import {fireEvent, render, screen, waitFor} from '@testing-library/react';
import App from './App';
import { setupWorker, rest } from 'msw'
import {setupServer} from "msw/node";

const server = setupServer(
    rest.get('http://localhost/cartData.json', (req, res, ctx) => {
      return res(
          ctx.json(
              [
                {
                  "Name": "Fancy Eyes",
                  "Price": 39.90,
                  "Quantity": 5
                },
                {
                  "Name": "Crazy Look",
                  "Price": 69.85,
                  "Quantity": 1
                },
                {
                  "Name": "Handsome Bird",
                  "Price": 199.99,
                  "Quantity": 2
                }
              ]              
          ),
      )
    }),
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())


test('cart is functional', async () => {
  render(<App />);
  // wait for cart to load
  await waitFor(() => screen.getByText('$199.99'));
  // check total
  screen.getByText('$669.33')
  const increaseButtons = screen.getAllByText('+');
  fireEvent.click(increaseButtons[1]);
  // check updated amount for 2nd entry
  screen.getByText('$139.70');
  // check updated total
  screen.getByText('$739.18');
  const deleteButtons = screen.getAllByText('x');
  fireEvent.click(deleteButtons[2]);
  // check updated total
  screen.getByText('$339.20');
  const decreaseButtons = screen.getAllByText('-');
  // item count decrease
  expect(decreaseButtons.length).toEqual(2);
  fireEvent.click(decreaseButtons[0]);
  // check updated total
  screen.getByText('$299.30');
});
