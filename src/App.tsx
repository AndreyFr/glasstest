import React from 'react';
import './App.css';
import {Cart} from "./Cart";

function App() {
  return (
    <div className="App">
      <header className="App-header">
          Test Shopping Cart
      </header>
      <Cart/>
    </div>
  );
}

export default App;
