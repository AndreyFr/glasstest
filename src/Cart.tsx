import {useEffect, useState} from "react";
import "./Cart.css";

class CartItemRecord
{
    public Name: string = '';
    public Price: number = 0;
    public Quantity: number = 0;
}

export function Cart()
{
    const [items, setItems] = useState<Array<CartItemRecord>>([]);
    const [error, setError] = useState("");
    
    const loadCart = () =>
    {
        fetch('cartData.json')
            .then( result => result.json())
            .then( jsonResult => setItems(jsonResult) )
            .catch( e => setError("Error loading data. Try again later."))
    };
    
    useEffect(() => 
    {
        loadCart();
    }, [])
    
    const changeQuantity = (idx: number, amount: number) =>
    {
        let newItems = items.concat();
        newItems[idx].Quantity = Math.max(0, newItems[idx].Quantity + amount);
        setItems(newItems);
    };
    
    const removeItem = (idx: number) => {
        let newItems = items.concat();
        newItems.splice(idx, 1);
        setItems(newItems);
    }
    
    return (
        <div className="cart">
            <h1>Shopping cart contents</h1>
            {error ? <div className="error">{error}</div> :
            items.length ?
                [
                    <div key="header" className="row header">
                        <div className="name">Product</div>
                        <div className="price">Price</div>
                        <div className="quantity">Quantity</div>
                        <div className="amount">Amount</div>
                        <div className="ops"></div>
                    </div>,
                    items.map( (i, idx) => 
                    <div key={`item${idx}`} className="row">
                        <div className="name">{i.Name}</div>
                        <div className="price">${i.Price}</div>
                        <div className="quantity">{i.Quantity}</div>
                        <div className="amount">${(i.Price * i.Quantity).toFixed(2)}</div>
                        <div className="ops">                    
                            <button title="Decrease quantity" onClick={() => changeQuantity(idx, -1)}>-</button>
                            <button title="Increase quantity" onClick={() => changeQuantity(idx, 1)}>+</button>
                            &nbsp;
                            <button title="Delete item  " onClick={() => removeItem(idx)}>x</button>
                        </div>
                    </div>),
                    <div key="total" className="row totalRow">
                        <div className="total">Total</div>
                        <div className="amount">
                            ${items.map(i => i.Price * i.Quantity).reduce((prev, current) => prev + current).toFixed(2)}
                        </div>
                        <div className="ops"/>
                    </div>
                ] : 
                "your cart is empty"}
        </div>
    );
}